from datetime import date
import os
import shutil


def criar_diretorio():
    global dir_path
    global log_path
    global d1

    today = date.today()
    d1 = today.strftime("%d-%m-%Y")
    dir_path = f"results/{d1}"
    log_path = f"logs/{d1}"

    # try:
    #     deletar_diretorio()
    # except:
    #     pass

    try:
        os.mkdir("results")
    except FileExistsError:
        pass

    try:
        os.mkdir("logs")
    except FileExistsError:
        pass

    try:
        os.mkdir(dir_path)
    except FileExistsError:
        pass

    try:
        os.mkdir(log_path)
    except FileExistsError:
        pass


def deletar_diretorio():
    folder = 'results/'
    try:
        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))
    except:
        pass


def salva_log(cpf, status, matricula=None):
    with open(f"{log_path}/{d1}.txt", "a+") as log_file:
        log_file.write(f"{cpf}/{matricula}: {status}\n")


def salva_request_content(filename, content):
    global dir_path
    today = date.today()
    d1 = today.strftime("%d-%m-%Y")
    dir_path = f"results/{d1}"
    with open(f'{dir_path}/{filename}', 'wb') as f:
        f.write(content)
