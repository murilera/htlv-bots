import requests
import time
import mysql.connector
import json
import helper
import os
import sys
import re
from bs4 import BeautifulSoup
from lxml import html


def _first_request():
    url = "https://www.hltv.org/results"

    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5'
    }

    first_request = s.get(url, headers=headers)
    helper.salva_request_content("results.html", first_request.content)

    return first_request.content


def main():
    helper.criar_diretorio()
    fr = _first_request()

    soup = BeautifulSoup(fr, "lxml")

    for a in soup.find_all('div', {"class": "result-con"}):
        for link in a.find_all('a', {"class": "a-reset"}):
            print(link.get('href'))

    return "Done"

if __name__ == "__main__":
    print("Iniciando Bot...")

    s = requests.Session()
    main()
