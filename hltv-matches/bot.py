import requests
import time
import mysql.connector
import json
import helper
import os
import sys
import re
from bs4 import BeautifulSoup
from lxml import html


def _first_request():
    url = "https://www.hltv.org/matches/2347597/dbl-poney-vs-navi-junior-european-development-championship-3-closed-qualifier"

    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5'
    }

    first_request = s.get(url, headers=headers)
    helper.salva_request_content("match.html", first_request.content)

    return first_request.content


def main():
    helper.criar_diretorio()
    fr = _first_request()

    soup = BeautifulSoup(fr, "lxml")

    # time = soup.find_all('div', ["timeAndEvent"])
    # for div in time:
    #     print(div.find('div', ["date"]).get_text())
    #     print(div.find('div', ["event text-ellipsis"]).find('a').get("title"))

    veto = soup.find_all('div', ["standard-box veto-box"])
    veto_first = veto[0].get_text()
    veto_first = veto_first.strip().split("\n")
    veto_first.remove('')
    print(veto_first)
    veto_second = veto[1].get_text()
    veto_second = veto_second.strip().split("\n")
    print(veto_second)

    flexbox = soup.find_all('div', ["mapholder"])
    # print(flexbox[1])
    for box in flexbox:
        print(box.find('div', ["mapname"]).get_text())
        # print(box.find_all('div', ["results-teamname text-ellipsis"]))
        for div in box.find_all('div', ["results-teamname text-ellipsis"]):
            print(div.get_text())

        # print(box.find_all('div', ["results-team-score"]))
        for div in box.find_all('div', ["results-team-score"]):
            print(div.get_text())

    stats = soup.find_all('a', {"class": "results-stats"})
    for link in stats:
        print(link.get("href"))

    # details = soup.find('div', ["small-padding stats-detailed-stats"])
    # print(details.find('a').get("href"))

    return "Done"

if __name__ == "__main__":
    print("Iniciando Bot...")

    s = requests.Session()
    main()
